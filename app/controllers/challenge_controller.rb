class ChallengeController < ApiController
  skip_before_filter :verify_authenticity_token
  before_filter :check_token!
  

  def get_challenges
    @challenges = @logged_user.challenges
    json @challenges
  end

  def challenge_player
    player = User.find_by_id(challenge_params[:player_id])

    # Check for an existing challenge
    @logged_user.challenges.each do |ch|
      if(ch.challengee_id==player.id)
        error 'You already have sent a challenge to this player.'
        return
      end
    end

    if(@logged_user.challenge player)
      json @logged_user.challenges
    else
      error 'Could not challenge player'
    end
  end

  def challenge_params
    params.permit(:player_id)
  end

end
