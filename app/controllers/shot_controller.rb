class ShotController < ApiController 
  skip_before_filter :verify_authenticity_token

  def get_shots_api
    @shots = @logged_user.shots
    json @shots
  end

  def post_shot_api
    @shot = Shot.create(shot_params)
    @shot.user = @logged_user
    post @shot
  end

  def shot_params
    params.permit(:caption, :s3url)
  end
  
end
