class UserController < ApiController

  skip_before_filter :check_token!

  # username - string
  # password - string
  # email - string
  # phone - string (optional)
  def register_api
    begin
      @user = User.create(register_api_params)
      post @user
    rescue ActiveRecord::RecordNotUnique
      error "The username #{register_api_params[:username].downcase} is taken. Please choose a different one."
    end
  end

  def admin_log_in
  end

  # username - string (optional)
  # password - string (optional)
  # auth_token - string (optional)
  def login_api
    @user = User.find_for_database_authentication({:username=>params[:username].downcase})

    if(!@user.nil?)
      if(!@user.valid_password?(params[:password]))
        @user = nil
      end
    end

    if(@user.nil?)
      @user = User.find_by_auth_token(params[:auth_token]) unless params[:auth_token].nil?
    else
      @user.generate_auth_token
    end

    if @user.nil?
      # Do nothing
      error "Your username or password was incorrect."
    else
      render json: @user
    end
  end

  def register_api_params
    params.permit(:username, :password, :email, :phone)
  end

  def login_api_params
    params.permit(:username, :password, :auth_token)
  end

end
